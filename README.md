Traefik docker service
=========

Ansible role to deploy traefik on a docker swarm cluster using docker-service role.


Role Variables
--------------

Same variables defined by docker-service, this role pre-configure some of them.

Dependencies
------------

* docker-service

Example Playbook
----------------

    - name: Deploy traefik
      include_role:
        name: traefik
      vars:
          service_state: "started"

License
-------

GPLv2 or later

Author Information
------------------

* Juan Luis Baptiste < juan _at_ juanbaptiste _dot_ tech >
* https://www.juanbaptiste.tech
